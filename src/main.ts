// import { enableProdMode } from '@angular/core';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
//
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {enableProdMode} from "@angular/core";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
if (environment.production) {
    enableProdMode();
}
//console.log(environment)

platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));
// import { importProvidersFrom } from '@angular/core';
// import { bootstrapApplication } from '@angular/platform-browser';
// import { RouterModule } from '@angular/router';
// import { AppComponent } from './app/app.component';
// import { routes } from './routes';
//
// bootstrapApplication(AppComponent, {
//     providers: [importProvidersFrom(RouterModule.forRoot(routes))],
// }).catch((err) => console.error(err));
