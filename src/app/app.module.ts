import { NgModule } from '@angular/core';
import {HashLocationStrategy, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppLayoutModule } from './layout/app.layout.module';
import {BrowserModule} from "@angular/platform-browser";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HomeModule} from "./works/components/page/home/home.module";
import {DynamicFormComponent} from "./works/components/dynamicComponent/dynamicFormComponent";
import {DynamicSelectComponent} from "./works/components/dynamicComponent/dynamicField/dynamicSelect/dynamicSelectComponent";
import {FieldsetModule} from "primeng/fieldset";
import {PanelHomeModule} from "./works/components/page/panel-home/panel-home.module";

@NgModule({
    declarations: [
        AppComponent
    ],
   imports: [
      AppRoutingModule,
      AppLayoutModule,
      BrowserModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      HomeModule,
      PanelHomeModule,
      DynamicFormComponent,
      DynamicSelectComponent,
      FieldsetModule,
   ],
    providers: [
        { provide: LocationStrategy, useClass: PathLocationStrategy }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
