import {Component, OnInit} from '@angular/core';
import {LazyLoadEvent} from "primeng/api";
import {PageViewOptionsType} from "../../domain/pageViewOptionsType";



export abstract class BaseSearchComponent  {
     constructor() {}

    // public abstract getVisibleColumnIds():string[];
    //
    // public abstract  getColumnHeaders():string[];

    public abstract getHeadersAndFields():object
    public abstract getFieldJsonData():object
    public abstract getPageViewOptions():PageViewOptionsType
}
