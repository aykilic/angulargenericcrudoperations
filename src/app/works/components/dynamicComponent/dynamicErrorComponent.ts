import { Component, Input, OnInit } from "@angular/core";
import {FormGroup, FormGroupDirective, ReactiveFormsModule} from "@angular/forms";
import {CommonModule, NgIf} from "@angular/common";

@Component({
    standalone:true,
    selector: "app-dynamic-error",
    templateUrl: "./dynamicErrorComponent.html",
    styles:[`
        .errorMessage {color:red};
        .alert{color:red};
    `],
    imports: [
        NgIf,
        CommonModule
    ]
})
export class DynamicErrorComponent implements OnInit {
    formName: FormGroup;
    @Input() fieldName: string;
    @Input() field:{
        type: "",
        value: "",
        label: "",
        validateLabel:"",
        rules: any,
    };

    constructor(private formgroupDirective: FormGroupDirective) {}

    ngOnInit() {
        this.formName = this.formgroupDirective.control;
    }
}
