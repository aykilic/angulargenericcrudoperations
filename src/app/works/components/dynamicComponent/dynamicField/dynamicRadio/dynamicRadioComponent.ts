import {AfterViewChecked, AfterViewInit, Component, Input} from "@angular/core";
import {FormGroup, FormGroupDirective, ReactiveFormsModule} from "@angular/forms";
import {CommonModule, NgForOf, NgIf} from "@angular/common";
import {RadioButtonModule} from "primeng/radiobutton";

@Component({
    standalone: true,
    selector: "app-dynamic-radio",
    templateUrl: "./dynamicRadioComponent.html",
    styles:[`
        .errorMessage {color:red};
        .alert{color:red};
    `],
    imports: [
        ReactiveFormsModule,
        RadioButtonModule,
        CommonModule
    ]
})
export class DynamicRadioComponent{
    @Input() field: any;
    @Input() fieldName:any;
    formName: FormGroup;

    constructor(private formgroupDirective: FormGroupDirective) {
        this.formName = formgroupDirective.control;

    }




    setValue(e: string) {
        if (e != null){
            // console.log(e)
        }
    }
}
