import {AfterViewInit, Component, Input, OnInit} from "@angular/core";
import {FormGroup, FormGroupDirective, ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {CommonModule} from "@angular/common";

@Component({
    standalone: true,
    selector: "app-dynamic-input",
    styles:[`
        .errorMessage {color:red};
        .alert{color:red};
    `],
    imports: [
        ReactiveFormsModule,
        InputTextModule,
        CommonModule
    ],
    templateUrl: "./dynamicInputComponent.html"


})
export class DynamicInputComponent implements AfterViewInit, OnInit{
    @Input() field: any;
    @Input() fieldName:any;


    formName: FormGroup;


    constructor(private formGroupDirective: FormGroupDirective) {
        this.formName = formGroupDirective.control;
    }

    ngOnInit() {
        // console.log("filed value", this.field.field.value);
        this.formName.updateValueAndValidity();
        // this.formName.valueChanges.subscribe(v=> {
        //     console.log('v',v);
        // })
    }


    ngAfterViewInit(): void {

        this.hasFieldError()
    }
    hasFieldError(): boolean {
        return ( this.formName.get(this.fieldName).invalid

        );
    }
}
