import {Component, Input, OnInit} from "@angular/core";
import {FormGroup, FormGroupDirective, ReactiveFormsModule} from "@angular/forms";
import {CheckboxModule} from "primeng/checkbox";
import {CommonModule, NgForOf, NgIf} from "@angular/common";

@Component({
    standalone: true,
    selector: "app-dynamic-checkbox",
    templateUrl: "./dynamicCheckboxComponent.html",
    styles:[`
        .errorMessage {color:red};
        .alert{color:red};
    `],
    imports: [
        ReactiveFormsModule,
        CheckboxModule,
        CommonModule
    ]
})
export class DynamicCheckboxComponent {
    @Input() field: any;
    @Input() fieldName:any;

    formName: any;

    constructor(private formgroupDirective: FormGroupDirective) {
        this.formName = formgroupDirective.control;
    }
}
