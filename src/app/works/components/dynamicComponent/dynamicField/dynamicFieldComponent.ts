import {ChangeDetectorRef, Component, Input, ViewChild, ViewContainerRef, AfterViewInit, OnInit} from "@angular/core";
import {FormGroup, FormGroupDirective} from "@angular/forms";
import {DynamicSelectComponent} from "./dynamicSelect/dynamicSelectComponent";
import {DynamicCheckboxComponent} from "./dynamicCheckbox/dynamicCheckboxComponent";
import {DynamicInputComponent} from "./dynamicInput/dynamicInputComponent";
import {DynamicRadioComponent} from "./dynamicRadio/dynamicRadioComponent";
import {DynamicDateComponent} from "./dynamicDate/dynamicDateComponent";

@Component({
    standalone:true,
    selector: "app-field-input",
    templateUrl: "./dynamicFieldComponent.html",
})
export class DynamicFieldComponent implements AfterViewInit, OnInit{

    supportedDynamicComponents = [
        {
            name: 'text',
            component: DynamicInputComponent
        },
        {
            name: 'number',
            component: DynamicInputComponent
        },
        {
            name: 'select',
            component: DynamicSelectComponent
        },
        {
            name: 'radio',
            component: DynamicRadioComponent
        },
        {
            name: 'date',
            component: DynamicDateComponent
        },
        {
            name: 'checkbox',
            component: DynamicCheckboxComponent
        },

    ]
    @ViewChild('dynamicInputContainer', { read: ViewContainerRef}) dynamicInputContainer!: ViewContainerRef;
    @Input() field: any;
    @Input() fieldName:any;

    formName: FormGroup;

    constructor(
        private formGroupDirective: FormGroupDirective,
        private cd: ChangeDetectorRef
    ) {

    }

    ngAfterViewInit(): void {
        this.registerDynamicField();

    }

    private registerDynamicField() {
        // console.log('this', this);
        this.dynamicInputContainer.clear();
        let componentInstance = this.getComponentByType(this.field.type)
        let dynamicComponent = this.dynamicInputContainer.createComponent(componentInstance)
        dynamicComponent.setInput('field', this.field);
        dynamicComponent.setInput('fieldName',this.fieldName)
        this.cd.detectChanges();
    }

    getComponentByType(type: string): any {
        // console.log('type', type);
        let componentDynamic = this.supportedDynamicComponents.find(c => c.name === type);
        return componentDynamic.component || DynamicInputComponent;
    }

    ngOnInit(): void {
        this.formName = this.formGroupDirective.control;
        // console.log("formName",this.formName)

    }

}
