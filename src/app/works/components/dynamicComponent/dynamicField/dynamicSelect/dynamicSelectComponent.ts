import {Component, Input} from "@angular/core";
import {FormGroup, FormGroupDirective, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule, NgForOf, NgIf} from "@angular/common";
import {DropdownModule} from "primeng/dropdown";

@Component({
    standalone: true,
    selector: "app-dynamic-select",
    styles:[`
        .errorMessage {color:red};
        .alert{color:red};
    `],
    imports: [
        ReactiveFormsModule,
        DropdownModule,
        FormsModule,
        CommonModule
    ],
    templateUrl: "./dynamicSelectComponent.html"

})
export class DynamicSelectComponent {
    @Input() field: any;
    @Input() fieldName:any;
    formName: FormGroup;

    constructor(private formgroupDirective: FormGroupDirective) {
        this.formName = formgroupDirective.control;
    }
}
