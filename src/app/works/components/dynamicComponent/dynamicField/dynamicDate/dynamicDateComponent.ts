import {Component, Input, OnInit} from "@angular/core";
import {FormGroup, FormGroupDirective, ReactiveFormsModule} from "@angular/forms";
import {CalendarModule} from "primeng/calendar"
import {CommonModule, NgIf} from "@angular/common";

@Component({
    standalone: true,
    selector: "app-dynamic-date",
    templateUrl: "./dynamicDateComponent.html",
    styles:[`
        .errorMessage {color:red};
        .alert{color:red};
    `],
    imports: [
        ReactiveFormsModule,
        CalendarModule,
        CommonModule
    ]
})
export class DynamicDateComponent implements OnInit{
    @Input() field: any;
    @Input() fieldName:any;
    formName: FormGroup;

    constructor(private formgroupDirective: FormGroupDirective) {
        this.formName = formgroupDirective.control;
    }

    ngOnInit(): void {
        console.log("date=");
        // console.log("");
    }
}
