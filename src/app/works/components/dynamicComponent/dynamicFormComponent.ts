import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, OnChanges} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {CommonModule} from "@angular/common";
import {PaginatorModule} from "primeng/paginator";
import {ButtonModule} from "primeng/button";
import {DynamicInputComponent} from "./dynamicField/dynamicInput/dynamicInputComponent";
import {DynamicFieldComponent} from "./dynamicField/dynamicFieldComponent";
import {PageViewOptionsType} from "../../domain/pageViewOptionsType";
import {range} from "lodash";


@Component({
    standalone: true,
    selector: 'app-dynamic-form',
    templateUrl: './dynamicFormComponent.html',
    imports: [
        CommonModule,
        ReactiveFormsModule,
        PaginatorModule,
        ButtonModule,
        DynamicInputComponent,
        DynamicFieldComponent
    ]
})
export class DynamicFormComponent implements OnInit {
    @Output() kaydetButton: EventEmitter<any> = new EventEmitter();
    @Input() model: any;
    @Input() getPageViewOptions:PageViewOptionsType;

    public dynamicFormGroup: FormGroup;
    public fields: any = [];


    ngOnInit() {
        // console.log('dynamicFormComponent ngOnInit');
        console.log("test",this.getPageViewOptions);
        // this.buildForm();

    }

    ngOnChanges() {
        // console.log('dynamicFormComponent ngOnChanges');
        // console.log('model', this.model);
        this.buildForm();
    }

    private buildForm() {
        this.fields = [];
        // console.log("build form");
        const formGroupFields: any = this.getFormControlsFields();
        this.dynamicFormGroup = new FormGroup(formGroupFields);
        // console.log(this.dynamicFormGroup)
    }

    private getFormControlsFields() {
        let formGroupFields: any = {};
        for (let field of Object.keys(this.model)) {

            let fieldProps = this.model[field];
            let validators = this.addValidator(fieldProps.rules);
            // console.log(fieldProps,field);

            formGroupFields[field] = new FormControl(fieldProps.value, validators);
            // console.log(formGroupFields)
            this.fields.push({...fieldProps, fieldName: field, field: fieldProps});
        }
        // console.log("formGroupFields",this.fields)
        return formGroupFields;
    }

    private addValidator(rules: any) {
        if (!rules) {
            return [];
        }

        let validators: any = [];

        // 1.seçenek---------------------

        // if (rules["required"]) validators.push(Validators.required);
        // validators.push(Validators.minLength(rules["minLength"]));

        // 2.seçenek----------------------
        Object.keys(rules).forEach((rule) => {
            switch (rule) {
                case "required":
                    if (rules[rule]==true) validators.push(Validators.required);
                    break;
                case "minLength":
                    validators.push(Validators.minLength(rules[rule]));
                    break;
                case "maxLength":
                    validators.push(Validators.maxLength(rules[rule]));
                    break;
                // Eklenebilecek diğer kural türleri buraya eklenebilir
            }
        });

        return validators;
    }

    onSubmit() {

    }
}
