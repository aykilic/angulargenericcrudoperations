import {
   AfterContentInit,
   AfterViewChecked,
   AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef,
   Component,
   DoCheck, EventEmitter,
   Input,
   OnChanges,
   OnInit, Output,
   SimpleChange,
   SimpleChanges, ViewChild, ViewContainerRef
} from "@angular/core";
import {HttpService} from "../../service/http.service";
import {customService} from "../../domain/customService";
import {LazyLoadEvent} from "primeng/api";
import {JsonFormData} from "../../domain/JsonFormData";
import * as _ from 'lodash';
import {PageViewOptionsType} from "../../domain/pageViewOptionsType";
import {Observable} from "rxjs";
import {DynamicFormComponent} from "../dynamicComponent/dynamicFormComponent";

@Component({
    selector:'app-table',
    templateUrl: './tableComponent.html',
    styleUrls: ['./style.scss']
})

export class TableComponent implements OnInit, OnChanges{
    @Output() kaydetButton :EventEmitter<any> = new EventEmitter();
    @Input() fieldData:JsonFormData;
    @Input() getPageViewOptions:PageViewOptionsType;
    getPageViewOption:PageViewOptionsType;
    fieldDatas:JsonFormData;
    @Input() columns : any;
    @Input() service:customService;
    @ViewChild(DynamicFormComponent) dynamicFormContainer!: DynamicFormComponent;
    selectedItem:object={};
    tableListArray : [];
    totalRecords:number;
    loading: boolean = true ;
    obj={field:"",header:""}
    cols:object[]=[];
    headers:string[]=[];
    event:LazyLoadEvent;
    _selectedColumns:any[];
     //open popup
    display:boolean=false;

    @Input() get selectedColumns(): any[] {
        return this._selectedColumns;
    }

    set selectedColumns(val: any[]) {
        //restore original order
        this._selectedColumns = this.cols.filter(col => val.includes(col));
    }
     constructor(private _http: HttpService) {
     }

    ngOnInit(): void {


        this.fieldDatas=this.fieldData
        this._http.get< any >(
            this.service.differentEndPoint,
            this.service.apiUrl,
            // this.service.apiUrl+"?_start=20&_end=30",
            ( res:any )=> {
                this.totalRecords=res.length
            });

        this.getPageViewOption=this.getPageViewOptions

       // this.dynamicFormContainer.ngOnInit()

    }


    getData(){

        this.loading=true

        let page:number = (this.event.first/this.event.rows)+1;
        let limit:number = this.event.rows;
        let sortField:string=this.event.sortField
        let sortOrder:string=this.event.sortOrder == 1 ? "asc" : "desc"
        let filters:any=Object.entries(this.event.filters)
            .filter(([key, value]) => value.value)
            .map(([key, value]) => `&${key}=${value.value}`)
            .join('');
        // console.log(filters)
            this._http.get< any >(
                this.service.differentEndPoint,
                // this.service.apiUrl,
                this.service.apiUrl+`?`+`_page=${page}&_limit=${limit}&_sort=${sortField}&_order=${sortOrder}${filters}`,
                ( res:any )=> {
                    this.listMethod(res)

                    this.loading=false
            })
    }
    listMethod(res:any){
        // header: ["A", "B", "C"],
        // field:  ["a", "b", "c"]
        this.tableListArray=res

        this.cols=[]

        let headerandfieldskey:Array<any>  =[]
        Object.keys(this.columns).forEach((key)=>{
            headerandfieldskey.push(key)
        })

        for (let i = 0; i < this.columns.header.length ; i++) {
            this.cols.push({[headerandfieldskey[0]]: this.columns.header[i],[headerandfieldskey[1]]: this.columns.field[i]});
        }
        this._selectedColumns=this.cols
    }


    loadItems(event: LazyLoadEvent) {
        this.event=event;
        this.getData()
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort in single sort mode
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec in single sort mode
        //multiSortMeta: An array of SortMeta objects used in multiple columns sorting. Each SortMeta has field and order properties.
        //filters: Filters object having field as key and filter value, filter matchMode as value
        //globalFilter: Value of the global filter if available
        //do a request to a remote datasource using a service and return the cars that match the lazy load criteria
    }

    onRowSelect($event: any) {
        // fieldata:[
        //     {    "id": 1,
        //          "first_name": "Horacio",
        //          "last_name": "Stamm",
        //          "email": "Nicklaus_Dickinson@yahoo.com"
        //      }
        //      ]
        // console.log("$event: any",$event.data);
        this.fieldDatas=this.fieldData
        Object.keys($event.data).forEach(key => {
            if (this.fieldDatas.hasOwnProperty(key)){
                this.fieldDatas[key].value = $event.data[key]
            }
        });
        this.display=true;
    }
    ekleButton(){
        this.fieldDatas = this.fieldData
        // console.log("fieldDatas=",this.fieldDatas);
        Object.keys(this.fieldData).forEach(key => {
            if (this.fieldDatas.hasOwnProperty(key)){
                this.fieldDatas[key].value = ""
            }
        });
        // console.log("ekle button3",this.fieldDatas);
        this.fieldDatas = _.cloneDeep(this.fieldDatas)
        this.display=true;
    }
    ngOnChanges(changes:SimpleChanges): void {
        console.log();
        // if (changes.fieldData) console.log("changes",changes.fieldData);
    }

   deneme() {
      console.log(this.dynamicFormContainer.dynamicFormGroup.value)
   }
}
