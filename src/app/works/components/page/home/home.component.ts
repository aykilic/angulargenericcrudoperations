
import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';


import { customService } from "../../../domain/customService";
import {BaseSearchComponent} from "../../baseComponent/BaseSearchComponent";
import {JsonFormData} from "../../../domain/JsonFormData";
import {PageViewOptionsType} from "../../../domain/pageViewOptionsType";
import {DynamicFormComponent} from "../../dynamicComponent/dynamicFormComponent";
import {TableComponent} from "../../tableComponent/tableComponent";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent extends BaseSearchComponent implements OnInit {

    public customService:customService | undefined
    @ViewChild(TableComponent,{static:true}) tableComponent!:TableComponent;
    // table data
    //-*-*-*-*-*-*-*-*-*-*-*//
    constructor( ) {
        super()
    }

    ngOnInit(): void {
        this.customService={
            apiUrl:'http://localhost:3000/users',
            differentEndPoint:true
        }
    }

    post(){
        // this._http.get<any>("todos",(res)=>console.log(res),false)
        //  this._http.get<any>("", "",(res)=>console.log(res),false)
        //  this._http.get<JsonFormData>("/assets/kurs/homeField.json",(res:JsonFormData)=> {
        //      this.model=res
        //     // console.log(res)
        // },true)

    }
    goster(obj:any){
        console.log(obj)
    }
    onUpload(e:any) {
        console.log(e);
    }

    // Sayfa Özelikleri
    override getPageViewOptions(): PageViewOptionsType {
      return <any>{
         layoutRow:"md:col-6 sm:col-12"
      };
   }
    // FieldData
    override getFieldJsonData(): JsonFormData {
        return <any>{
            "id": {
                "type": "text",
                "value": "",
                "label": "id",
                "rules": {
                    "required": true
                }
            },
            "first_name": {
                "type": "text",
                "value": "",
                "label": "Adı",
                "rules": {
                    "minLength": 2
                }
            },
            "last_name": {
                "type": "text",
                "value": "",
                "label": "Soyadı"
            },
            // "email": {
            //     "type": "text",
            //     "value": "",
            //     "label": "Email"
            // },
            "typeBussines": {
                "type": "radio",
                "label": "Medeni Durum",
                "value": "",
                "options": [
                    {
                        "label": "Evli",
                        "value": "Evli"
                    },
                    {
                        "label": "Bekar",
                        "value": "Bekar"
                    }
                ]
            },
            "suscriptionType": {
                "type": "select",
                "label": "Üyelik Tipi",
                "value": "",
                "rules": {
                    "required": false
                },
                "options": [
                    {
                        "label": "Pick one",
                        "value": "medium"
                    },
                    {
                        "label": "Premium",
                        "value": "premium"
                    },
                    {
                        "label": "Basic",
                        "value": "basic"
                    }
                ]
            },
            "secim": {
                "type": "checkbox",
                "label": "1. Seçenek",
                "value": "",
                "options": [
                    {
                        "label": "Enterprise",
                        "value": "1"
                    },
                    {
                        "label": "Home",
                        "value": "2"
                    }
                ],
                "rules": {
                    "required": false
                }
            },
            "tarih":{
                "type": "date",
                "label": "Başlangıç Tarihi",
                "value": ""
            }
            //  'min'
            //  'max'
            //  'required'
            //  'requiredTrue'
            //  'email'
            //  'minLength'
            //  'maxLength'
            //  'pattern'
            //  'nullValidator'

        }
    }
    // TableData
    override getHeadersAndFields(): object {
        return {
            header: ["id", "Adı", "Soyadı","Email"],
            field:  ["id", "first_name", "last_name","email"]
        };
    }
}
