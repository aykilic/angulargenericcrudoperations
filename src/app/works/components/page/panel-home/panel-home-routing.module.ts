import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import {PanelHomeComponent} from "./panel-home.component";



@NgModule({
  declarations: [],
   imports: [RouterModule.forChild([
      { path: '', component: PanelHomeComponent }
   ])],
})
export class PanelHomeRoutingModule { }
