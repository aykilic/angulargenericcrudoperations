import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PanelHomeRoutingModule} from "./panel-home-routing.module";
import {PanelHomeComponent} from "./panel-home.component";
import {FieldsetModule} from "primeng/fieldset";



@NgModule({
  declarations: [PanelHomeComponent],
   imports: [
      CommonModule,
      PanelHomeRoutingModule,
      FieldsetModule
   ]
})
export class PanelHomeModule { }
