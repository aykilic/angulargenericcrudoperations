import {Injectable} from "@angular/core";
import {OgrenimDurumuEnum} from "../domain/ogrenimDurumEnum";

@Injectable()
export class OgrenimDurumuService {

    constructor(private ogrenimDurumu: OgrenimDurumuEnum,) { }

    getOgrenimDurumu() {
        return this.ogrenimDurumu
    }
}
