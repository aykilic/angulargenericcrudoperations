import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class OgrenimDurumuService {

    constructor(private http: HttpClient) { }

    getOgrenimDurumu() {
        return this.http.get<any>('assets/kurs/data/ogrenimDurumuEnum.json')
            .toPromise()
            .then(res => res.data as any)
            .then(data => data);
    }
}
