import {Injectable} from "@angular/core";
import {HttpClient, HttpEvent} from "@angular/common/http";
import {environment} from "../../../environments/environment";


@Injectable({
    providedIn:'root'
})
export class HttpService {

    constructor(

        private _http:HttpClient

    ) {}

    api: string=environment.api

    setApi( differentEndPoint: boolean, api:string){
        if (differentEndPoint)
            return api;
        else
            return this.api+api;
    }
    get<T>( differentEndPoint: boolean=false,  api: string, callBack: (res: HttpEvent<T>)=> void,options:any={}){

       let apiUrl = this.setApi( differentEndPoint, api);

       this._http.get<T>(apiUrl, options).subscribe({

           next:(res)=>callBack(res)
       })
    }
    post<T>( differentEndPoint: boolean=false, options:any={}, model:any, api: string, callBack: (res: HttpEvent<T>)=> void){

       let apiUrl = this.setApi( differentEndPoint, api);

       this._http.post<T>(apiUrl, model, options).subscribe({

           next:(res)=>callBack(res)
       })
    }
    delete<T>(api: string, callBack: (res: HttpEvent<T>)=> void, differentEndPoint: boolean=false, options:any={}){

        let apiUrl = this.setApi( differentEndPoint, api);

        this._http.delete<T>(apiUrl, options).subscribe({

            next:(res)=>callBack(res)
        })
    }
    // options: {
    //     headers?: HttpHeaders | {[header: string]: string | string[]},
    //     observe?: 'body' | 'events' | 'response',
    //     params?: HttpParams|{[param: string]: string | number | boolean | ReadonlyArray<string | number | boolean>},
    //     reportProgress?: boolean,
    //     responseType?: 'arraybuffer'|'blob'|'json'|'text',
    //     withCredentials?: boolean,
    // }
}
