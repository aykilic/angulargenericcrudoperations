import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {User} from "../domain/user";


@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    getUser() {
        return this.http.get<any>('assets/kurs/data/users.json')
            .toPromise()
            .then(res => res.data as User)
            .then(data => data);
    }
}
