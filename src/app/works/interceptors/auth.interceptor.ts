import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    constructor(){}
    intercept( request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        request = request.clone({
            headers: request.headers.set("Authorization", "Bearer " + "token bilgisi gelecek")
        })
        return next.handle(request);
    }


}
