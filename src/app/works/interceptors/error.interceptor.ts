import {Injectable} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor{
    constructor(){}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        return next.handle(request).pipe(
            catchError((err: HttpErrorResponse)=>{
                switch(err.status){
                    case 404:
                        console.log("Api Url'si Yanlış");
                        break;
                    default:
                        console.log(err);
                        break;
                }
                return of()
            })
        )
    }
}

