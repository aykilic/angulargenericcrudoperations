export enum OgrenimDurumuEnum{
    OKURYAZAR = "Okur-Yazar",
    ILKOKUL = "ilkokul",
    ILKOGRETIM = "İlköğretim",
    LISE = "Lise",
    ONLISANS = "Önlisans",
    LISANS = "Lisans",
    YUKSEKLISANS = "Yüksek Lisans",
    DOKTORA = "Doktora"
}

