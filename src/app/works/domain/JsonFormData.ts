interface JsonFormValidators {
    min?: number;
    max?: number;
    required?: boolean;
    requiredTrue?: boolean;
    email?: boolean;
    minLength?: boolean;
    maxLength?: boolean;
    pattern?: string;
    nullValidator?: boolean;
}
interface JsonFormControlOptions {
    min?: string;
    max?: string;
    step?: string;
    icon?: string;
}
interface JsonFormControls {
    label: string;
    value: string;
    type: string;
    options?: JsonFormControlOptions;
    rules?: JsonFormValidators;
    validators?: JsonFormValidators;
}
export interface JsonFormData {
    [key:string]: JsonFormControls
}
