export interface User {
    id?: Number;
    adiSoyadi?: String;
    tcKimlikNo?: Number;
    dogumYeri?: String;
    dogumTarihi?: Date;
    babaAdi?: String;
    anaAdi?: String;
    medeniHali?: String;
    cinsiyeti?: String;
}
