import { __decorate } from "tslib";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
let HttpService = class HttpService {
    constructor(_http) {
        this._http = _http;
        this.api = environment.api;
    }
    setApi(differentEndPoint, api) {
        if (differentEndPoint)
            return api;
        else
            return this.api + api;
    }
    get(differentEndPoint = false, api, callBack, options = {}) {
        let apiUrl = this.setApi(differentEndPoint, api);
        this._http.get(apiUrl, options).subscribe({
            next: (res) => callBack(res)
        });
    }
    post(differentEndPoint = false, options = {}, model, api, callBack) {
        let apiUrl = this.setApi(differentEndPoint, api);
        this._http.post(apiUrl, model, options).subscribe({
            next: (res) => callBack(res)
        });
    }
    delete(api, callBack, differentEndPoint = false, options = {}) {
        let apiUrl = this.setApi(differentEndPoint, api);
        this._http.delete(apiUrl, options).subscribe({
            next: (res) => callBack(res)
        });
    }
};
HttpService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], HttpService);
export { HttpService };
//# sourceMappingURL=http.service.js.map