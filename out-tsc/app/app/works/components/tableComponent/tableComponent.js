import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from "@angular/core";
import * as _ from 'lodash';
let TableComponent = class TableComponent {
    constructor(_http) {
        this._http = _http;
        this.kaydetButton = new EventEmitter();
        this.selectedItem = {};
        this.loading = true;
        this.obj = { field: "", header: "" };
        this.cols = [];
        this.headers = [];
        //open popup
        this.display = false;
    }
    get selectedColumns() {
        return this._selectedColumns;
    }
    set selectedColumns(val) {
        //restore original order
        this._selectedColumns = this.cols.filter(col => val.includes(col));
    }
    ngOnInit() {
        this.fieldDatas = this.fieldData;
        this._http.get(this.service.differentEndPoint, this.service.apiUrl, 
        // this.service.apiUrl+"?_start=20&_end=30",
        (res) => {
            this.totalRecords = res.length;
        });
    }
    getData() {
        this.loading = true;
        let page = (this.event.first / this.event.rows) + 1;
        let limit = this.event.rows;
        let sortField = this.event.sortField;
        let sortOrder = this.event.sortOrder == 1 ? "asc" : "desc";
        let filters = Object.entries(this.event.filters)
            .filter(([key, value]) => value.value)
            .map(([key, value]) => `&${key}=${value.value}`)
            .join('');
        // console.log(filters)
        this._http.get(this.service.differentEndPoint, 
        // this.service.apiUrl,
        this.service.apiUrl + `?` + `_page=${page}&_limit=${limit}&_sort=${sortField}&_order=${sortOrder}${filters}`, (res) => {
            this.listMethod(res);
            this.loading = false;
        });
    }
    listMethod(res) {
        // header: ["A", "B", "C"],
        // field:  ["a", "b", "c"]
        this.tableListArray = res;
        this.cols = [];
        let headerandfieldskey = [];
        Object.keys(this.columns).forEach((key) => {
            headerandfieldskey.push(key);
        });
        for (let i = 0; i < this.columns.header.length; i++) {
            this.cols.push({ [headerandfieldskey[0]]: this.columns.header[i], [headerandfieldskey[1]]: this.columns.field[i] });
        }
        this._selectedColumns = this.cols;
    }
    loadItems(event) {
        this.event = event;
        this.getData();
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort in single sort mode
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec in single sort mode
        //multiSortMeta: An array of SortMeta objects used in multiple columns sorting. Each SortMeta has field and order properties.
        //filters: Filters object having field as key and filter value, filter matchMode as value
        //globalFilter: Value of the global filter if available
        //do a request to a remote datasource using a service and return the cars that match the lazy load criteria
    }
    onRowSelect($event) {
        // fieldata:[
        //     {    "id": 1,
        //          "first_name": "Horacio",
        //          "last_name": "Stamm",
        //          "email": "Nicklaus_Dickinson@yahoo.com"
        //      }
        //      ]
        // console.log("$event: any",$event.data);
        this.fieldDatas = this.fieldData;
        Object.keys($event.data).forEach(key => {
            if (this.fieldDatas.hasOwnProperty(key)) {
                this.fieldDatas[key].value = $event.data[key];
            }
        });
        this.display = true;
    }
    ekleButton() {
        this.fieldDatas = this.fieldData;
        // console.log("fieldDatas=",this.fieldDatas);
        Object.keys(this.fieldData).forEach(key => {
            if (this.fieldDatas.hasOwnProperty(key)) {
                this.fieldDatas[key].value = "";
            }
        });
        // console.log("ekle button3",this.fieldDatas);
        this.fieldDatas = _.cloneDeep(this.fieldDatas);
        this.display = true;
    }
    ngOnChanges(changes) {
        console.log();
        // if (changes.fieldData) console.log("changes",changes.fieldData);
    }
};
__decorate([
    Output()
], TableComponent.prototype, "kaydetButton", void 0);
__decorate([
    Input()
], TableComponent.prototype, "fieldData", void 0);
__decorate([
    Input()
], TableComponent.prototype, "columns", void 0);
__decorate([
    Input()
], TableComponent.prototype, "service", void 0);
__decorate([
    Input()
], TableComponent.prototype, "selectedColumns", null);
TableComponent = __decorate([
    Component({
        selector: 'app-table',
        templateUrl: './tableComponent.html',
        styleUrls: ['./style.scss']
    })
], TableComponent);
export { TableComponent };
//# sourceMappingURL=tableComponent.js.map