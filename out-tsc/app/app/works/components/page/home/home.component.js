import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { BaseSearchComponent } from "../../baseComponent/BaseSearchComponent";
let HomeComponent = class HomeComponent extends BaseSearchComponent {
    // table data
    //-*-*-*-*-*-*-*-*-*-*-*//
    constructor() {
        super();
    }
    ngOnInit() {
        this.customService = {
            apiUrl: 'http://localhost:3000/users',
            differentEndPoint: true
        };
    }
    post() {
        // this._http.get<any>("todos",(res)=>console.log(res),false)
        //  this._http.get<any>("", "",(res)=>console.log(res),false)
        //  this._http.get<JsonFormData>("/assets/kurs/homeField.json",(res:JsonFormData)=> {
        //      this.model=res
        //     // console.log(res)
        // },true)
    }
    goster(obj) {
        console.log(obj);
    }
    onUpload(e) {
        console.log(e);
    }
    // Sayfa Özelikleri
    getPageViewOptions() {
        return undefined;
    }
    // FieldData
    getFieldJsonData() {
        return {
            "id": {
                "type": "text",
                "value": "",
                "label": "id",
                "rules": {
                    "required": true
                }
            },
            "first_name": {
                "type": "text",
                "value": "",
                "label": "Adı",
                "rules": {
                    "minLength": 2
                }
            },
            "last_name": {
                "type": "text",
                "value": "",
                "label": "Soyadı"
            },
            // "email": {
            //     "type": "text",
            //     "value": "",
            //     "label": "Email"
            // },
            "typeBussines": {
                "type": "radio",
                "label": "Medeni Durum",
                "value": "",
                "options": [
                    {
                        "label": "Evli",
                        "value": "Evli"
                    },
                    {
                        "label": "Bekar",
                        "value": "Bekar"
                    }
                ]
            },
            "suscriptionType": {
                "type": "select",
                "label": "Üyelik Tipi",
                "value": "",
                "rules": {
                    "required": false
                },
                "options": [
                    {
                        "label": "Pick one",
                        "value": "medium"
                    },
                    {
                        "label": "Premium",
                        "value": "premium"
                    },
                    {
                        "label": "Basic",
                        "value": "basic"
                    }
                ]
            },
            "secim": {
                "type": "checkbox",
                "label": "1. Seçenek",
                "value": "",
                "options": [
                    {
                        "label": "Enterprise",
                        "value": "1"
                    },
                    {
                        "label": "Home",
                        "value": "2"
                    }
                ],
                "rules": {
                    "required": false
                }
            },
            "tarih": {
                "type": "date",
                "label": "Başlangıç Tarihi",
                "value": ""
            }
            //  'min'
            //  'max'
            //  'required'
            //  'requiredTrue'
            //  'email'
            //  'minLength'
            //  'maxLength'
            //  'pattern'
            //  'nullValidator'
        };
    }
    //TableData
    getHeadersAndFields() {
        return {
            header: ["id", "Adı", "Soyadı", "Email"],
            field: ["id", "first_name", "last_name", "email"]
        };
    }
};
HomeComponent = __decorate([
    Component({
        selector: 'app-home',
        templateUrl: './home.component.html'
    })
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map