import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from "./home-routing.module";
import { ButtonModule } from "primeng/button";
import { FormsModule } from "@angular/forms";
import { InputTextModule } from "primeng/inputtext";
import { HttpClientModule } from "@angular/common/http";
import { TableModule } from "primeng/table";
import { TableComponent } from "../../tableComponent/tableComponent";
import { DynamicFormComponent } from "../../dynamicComponent/dynamicFormComponent";
import { MultiSelectModule } from "primeng/multiselect";
import { StyleClassModule } from "primeng/styleclass";
import { RippleModule } from "primeng/ripple";
import { TooltipModule } from "primeng/tooltip";
import { CardModule } from "primeng/card";
import { DialogModule } from "primeng/dialog";
import { PanelModule } from "primeng/panel";
let HomeModule = class HomeModule {
};
HomeModule = __decorate([
    NgModule({
        declarations: [HomeComponent, TableComponent, TableComponent],
        imports: [
            CommonModule,
            HomeRoutingModule,
            DynamicFormComponent,
            ButtonModule,
            FormsModule,
            InputTextModule,
            HttpClientModule,
            TableModule,
            MultiSelectModule,
            StyleClassModule,
            RippleModule,
            TooltipModule,
            CardModule,
            DialogModule,
            PanelModule
        ]
    })
], HomeModule);
export { HomeModule };
//# sourceMappingURL=home.module.js.map