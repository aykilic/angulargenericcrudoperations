import { __decorate } from "tslib";
import { Component, Input } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DropdownModule } from "primeng/dropdown";
let DynamicSelectComponent = class DynamicSelectComponent {
    constructor(formgroupDirective) {
        this.formgroupDirective = formgroupDirective;
        this.formName = formgroupDirective.control;
    }
};
__decorate([
    Input()
], DynamicSelectComponent.prototype, "field", void 0);
__decorate([
    Input()
], DynamicSelectComponent.prototype, "fieldName", void 0);
DynamicSelectComponent = __decorate([
    Component({
        standalone: true,
        selector: "app-dynamic-select",
        styles: [`
        .errorMessage {color:red};
        .alert{color:red};
    `],
        imports: [
            ReactiveFormsModule,
            DropdownModule,
            FormsModule,
            CommonModule
        ],
        templateUrl: "./dynamicSelectComponent.html"
    })
], DynamicSelectComponent);
export { DynamicSelectComponent };
//# sourceMappingURL=dynamicSelectComponent.js.map