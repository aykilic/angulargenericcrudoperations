import { __decorate } from "tslib";
import { Component, Input } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { CalendarModule } from "primeng/calendar";
import { CommonModule } from "@angular/common";
let DynamicDateComponent = class DynamicDateComponent {
    constructor(formgroupDirective) {
        this.formgroupDirective = formgroupDirective;
        this.formName = formgroupDirective.control;
    }
    ngOnInit() {
        console.log("date=", this.field);
    }
};
__decorate([
    Input()
], DynamicDateComponent.prototype, "field", void 0);
__decorate([
    Input()
], DynamicDateComponent.prototype, "fieldName", void 0);
DynamicDateComponent = __decorate([
    Component({
        standalone: true,
        selector: "app-dynamic-date",
        templateUrl: "./dynamicDateComponent.html",
        styles: [`
        .errorMessage {color:red};
        .alert{color:red};
    `],
        imports: [
            ReactiveFormsModule,
            CalendarModule,
            CommonModule
        ]
    })
], DynamicDateComponent);
export { DynamicDateComponent };
//# sourceMappingURL=dynamicDateComponent.js.map