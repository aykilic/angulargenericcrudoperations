import { __decorate } from "tslib";
import { Component, Input } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { InputTextModule } from "primeng/inputtext";
import { CommonModule } from "@angular/common";
let DynamicInputComponent = class DynamicInputComponent {
    constructor(formGroupDirective) {
        this.formGroupDirective = formGroupDirective;
        this.formName = formGroupDirective.control;
    }
    ngOnInit() {
        // console.log("filed value", this.field.field.value);
        this.formName.updateValueAndValidity();
        // this.formName.valueChanges.subscribe(v=> {
        //     console.log('v',v);
        // })
    }
    ngAfterViewInit() {
        this.hasFieldError();
    }
    hasFieldError() {
        return (this.formName.get(this.fieldName).invalid);
    }
};
__decorate([
    Input()
], DynamicInputComponent.prototype, "field", void 0);
__decorate([
    Input()
], DynamicInputComponent.prototype, "fieldName", void 0);
DynamicInputComponent = __decorate([
    Component({
        standalone: true,
        selector: "app-dynamic-input",
        styles: [`
        .errorMessage {color:red};
        .alert{color:red};
    `],
        imports: [
            ReactiveFormsModule,
            InputTextModule,
            CommonModule
        ],
        templateUrl: "./dynamicInputComponent.html"
    })
], DynamicInputComponent);
export { DynamicInputComponent };
//# sourceMappingURL=dynamicInputComponent.js.map