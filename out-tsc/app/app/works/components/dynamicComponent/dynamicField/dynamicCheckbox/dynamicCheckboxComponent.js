import { __decorate } from "tslib";
import { Component, Input } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { CheckboxModule } from "primeng/checkbox";
import { CommonModule } from "@angular/common";
let DynamicCheckboxComponent = class DynamicCheckboxComponent {
    constructor(formgroupDirective) {
        this.formgroupDirective = formgroupDirective;
        this.formName = formgroupDirective.control;
    }
};
__decorate([
    Input()
], DynamicCheckboxComponent.prototype, "field", void 0);
__decorate([
    Input()
], DynamicCheckboxComponent.prototype, "fieldName", void 0);
DynamicCheckboxComponent = __decorate([
    Component({
        standalone: true,
        selector: "app-dynamic-checkbox",
        templateUrl: "./dynamicCheckboxComponent.html",
        styles: [`
        .errorMessage {color:red};
        .alert{color:red};
    `],
        imports: [
            ReactiveFormsModule,
            CheckboxModule,
            CommonModule
        ]
    })
], DynamicCheckboxComponent);
export { DynamicCheckboxComponent };
//# sourceMappingURL=dynamicCheckboxComponent.js.map