import { __decorate } from "tslib";
import { Component, Input, ViewChild, ViewContainerRef } from "@angular/core";
import { DynamicSelectComponent } from "./dynamicSelect/dynamicSelectComponent";
import { DynamicCheckboxComponent } from "./dynamicCheckbox/dynamicCheckboxComponent";
import { DynamicInputComponent } from "./dynamicInput/dynamicInputComponent";
import { DynamicRadioComponent } from "./dynamicRadio/dynamicRadioComponent";
import { DynamicDateComponent } from "./dynamicDate/dynamicDateComponent";
let DynamicFieldComponent = class DynamicFieldComponent {
    constructor(formGroupDirective, cd) {
        this.formGroupDirective = formGroupDirective;
        this.cd = cd;
        this.supportedDynamicComponents = [
            {
                name: 'text',
                component: DynamicInputComponent
            },
            {
                name: 'number',
                component: DynamicInputComponent
            },
            {
                name: 'select',
                component: DynamicSelectComponent
            },
            {
                name: 'radio',
                component: DynamicRadioComponent
            },
            {
                name: 'date',
                component: DynamicDateComponent
            },
            {
                name: 'checkbox',
                component: DynamicCheckboxComponent
            },
        ];
    }
    ngAfterViewInit() {
        this.registerDynamicField();
    }
    registerDynamicField() {
        // console.log('this', this);
        this.dynamicInputContainer.clear();
        let componentInstance = this.getComponentByType(this.field.type);
        let dynamicComponent = this.dynamicInputContainer.createComponent(componentInstance);
        dynamicComponent.setInput('field', this.field);
        dynamicComponent.setInput('fieldName', this.fieldName);
        this.cd.detectChanges();
    }
    getComponentByType(type) {
        // console.log('type', type);
        let componentDynamic = this.supportedDynamicComponents.find(c => c.name === type);
        return componentDynamic.component || DynamicInputComponent;
    }
    ngOnInit() {
        this.formName = this.formGroupDirective.control;
        // console.log("formName",this.formName)
    }
};
__decorate([
    ViewChild('dynamicInputContainer', { read: ViewContainerRef })
], DynamicFieldComponent.prototype, "dynamicInputContainer", void 0);
__decorate([
    Input()
], DynamicFieldComponent.prototype, "field", void 0);
__decorate([
    Input()
], DynamicFieldComponent.prototype, "fieldName", void 0);
DynamicFieldComponent = __decorate([
    Component({
        standalone: true,
        selector: "app-field-input",
        templateUrl: "./dynamicFieldComponent.html",
    })
], DynamicFieldComponent);
export { DynamicFieldComponent };
//# sourceMappingURL=dynamicFieldComponent.js.map