import { __decorate } from "tslib";
import { Component, Input } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RadioButtonModule } from "primeng/radiobutton";
let DynamicRadioComponent = class DynamicRadioComponent {
    constructor(formgroupDirective) {
        this.formgroupDirective = formgroupDirective;
        this.formName = formgroupDirective.control;
    }
    setValue(e) {
        if (e != null) {
            // console.log(e)
        }
    }
};
__decorate([
    Input()
], DynamicRadioComponent.prototype, "field", void 0);
__decorate([
    Input()
], DynamicRadioComponent.prototype, "fieldName", void 0);
DynamicRadioComponent = __decorate([
    Component({
        standalone: true,
        selector: "app-dynamic-radio",
        templateUrl: "./dynamicRadioComponent.html",
        styles: [`
        .errorMessage {color:red};
        .alert{color:red};
    `],
        imports: [
            ReactiveFormsModule,
            RadioButtonModule,
            CommonModule
        ]
    })
], DynamicRadioComponent);
export { DynamicRadioComponent };
//# sourceMappingURL=dynamicRadioComponent.js.map