import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from "@angular/common";
import { PaginatorModule } from "primeng/paginator";
import { ButtonModule } from "primeng/button";
import { DynamicInputComponent } from "./dynamicField/dynamicInput/dynamicInputComponent";
import { DynamicFieldComponent } from "./dynamicField/dynamicFieldComponent";
let DynamicFormComponent = class DynamicFormComponent {
    constructor() {
        this.kaydetButton = new EventEmitter();
        this.fields = [];
    }
    ngOnInit() {
        // console.log('dynamicFormComponent ngOnInit');
        console.log("test");
        //this.buildForm();
    }
    ngOnChanges() {
        // console.log('dynamicFormComponent ngOnChanges');
        // console.log('model', this.model);
        this.buildForm();
    }
    buildForm() {
        this.fields = [];
        // console.log("build form");
        const formGroupFields = this.getFormControlsFields();
        this.dynamicFormGroup = new FormGroup(formGroupFields);
        // console.log(this.dynamicFormGroup)
    }
    getFormControlsFields() {
        let formGroupFields = {};
        for (let field of Object.keys(this.model)) {
            let fieldProps = this.model[field];
            let validators = this.addValidator(fieldProps.rules);
            // console.log(fieldProps,field);
            formGroupFields[field] = new FormControl(fieldProps.value, validators);
            // console.log(formGroupFields)
            this.fields.push(Object.assign(Object.assign({}, fieldProps), { fieldName: field, field: fieldProps }));
        }
        console.log("formGroupFields", this.fields);
        return formGroupFields;
    }
    addValidator(rules) {
        if (!rules) {
            return [];
        }
        let validators = [];
        // 1.seçenek---------------------
        if (rules["required"])
            validators.push(Validators.required);
        validators.push(Validators.minLength(rules["minLength"]));
        // 2.seçenek----------------------
        // Object.keys(rules).forEach((rule) => {
        //     switch (rule) {
        //         case "required":
        //             if (rules[rule]==true) validators.push(Validators.required);
        //             break;
        //         case "minLength":
        //             validators.push(Validators.minLength(rules[rule]));
        //             break;
        //         case "maxLength":
        //             validators.push(Validators.maxLength(rules[rule]));
        //             break;
        //         // Eklenebilecek diğer kural türleri buraya eklenebilir
        //     }
        // });
        return validators;
    }
    onSubmit() {
    }
};
__decorate([
    Output()
], DynamicFormComponent.prototype, "kaydetButton", void 0);
__decorate([
    Input()
], DynamicFormComponent.prototype, "model", void 0);
DynamicFormComponent = __decorate([
    Component({
        standalone: true,
        selector: 'app-dynamic-form',
        templateUrl: './dynamicFormComponent.html',
        imports: [
            CommonModule,
            ReactiveFormsModule,
            PaginatorModule,
            ButtonModule,
            DynamicInputComponent,
            DynamicFieldComponent
        ]
    })
], DynamicFormComponent);
export { DynamicFormComponent };
//# sourceMappingURL=dynamicFormComponent.js.map