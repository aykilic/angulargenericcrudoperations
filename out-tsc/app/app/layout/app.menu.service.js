import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
let MenuService = class MenuService {
    constructor() {
        this.menuSource = new Subject();
        this.resetSource = new Subject();
        this.menuSource$ = this.menuSource.asObservable();
        this.resetSource$ = this.resetSource.asObservable();
    }
    onMenuStateChange(event) {
        this.menuSource.next(event);
    }
    reset() {
        this.resetSource.next(true);
    }
};
MenuService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], MenuService);
export { MenuService };
//# sourceMappingURL=app.menu.service.js.map