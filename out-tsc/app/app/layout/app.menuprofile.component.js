import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
let AppMenuProfileComponent = class AppMenuProfileComponent {
    constructor(layoutService, el) {
        this.layoutService = layoutService;
        this.el = el;
    }
    toggleMenu() {
        this.layoutService.onMenuProfileToggle();
    }
    get isHorizontal() {
        return this.layoutService.isHorizontal() && this.layoutService.isDesktop();
    }
    get menuProfileActive() {
        return this.layoutService.state.menuProfileActive;
    }
    get menuProfilePosition() {
        return this.layoutService.config.menuProfilePosition;
    }
    get isTooltipDisabled() {
        return !this.layoutService.isSlim();
    }
};
AppMenuProfileComponent = __decorate([
    Component({
        selector: 'app-menu-profile',
        templateUrl: './app.menuprofile.component.html',
        animations: [
            trigger('menu', [
                transition('void => inline', [
                    style({ height: 0 }),
                    animate('400ms cubic-bezier(0.86, 0, 0.07, 1)', style({ opacity: 1, height: '*' })),
                ]),
                transition('inline => void', [
                    animate('400ms cubic-bezier(0.86, 0, 0.07, 1)', style({ opacity: 0, height: '0' }))
                ]),
                transition('void => overlay', [
                    style({ opacity: 0, transform: 'scaleY(0.8)' }),
                    animate('.12s cubic-bezier(0, 0, 0.2, 1)')
                ]),
                transition('overlay => void', [
                    animate('.1s linear', style({ opacity: 0 }))
                ])
            ])
        ]
    })
], AppMenuProfileComponent);
export { AppMenuProfileComponent };
//# sourceMappingURL=app.menuprofile.component.js.map