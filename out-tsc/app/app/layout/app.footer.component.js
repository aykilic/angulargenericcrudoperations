import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AppFooterComponent = class AppFooterComponent {
    constructor(layoutService) {
        this.layoutService = layoutService;
    }
};
AppFooterComponent = __decorate([
    Component({
        selector: 'app-footer',
        templateUrl: './app.footer.component.html'
    })
], AppFooterComponent);
export { AppFooterComponent };
//# sourceMappingURL=app.footer.component.js.map