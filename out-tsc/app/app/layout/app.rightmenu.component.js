import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AppRightMenuComponent = class AppRightMenuComponent {
    constructor(layoutService) {
        this.layoutService = layoutService;
    }
    get rightMenuActive() {
        return this.layoutService.state.rightMenuActive;
    }
    set rightMenuActive(_val) {
        this.layoutService.state.rightMenuActive = _val;
    }
};
AppRightMenuComponent = __decorate([
    Component({
        selector: 'app-rightmenu',
        templateUrl: './app.rightmenu.component.html'
    })
], AppRightMenuComponent);
export { AppRightMenuComponent };
//# sourceMappingURL=app.rightmenu.component.js.map