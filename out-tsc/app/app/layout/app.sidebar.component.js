import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { AppMenuProfileComponent } from './app.menuprofile.component';
let AppSidebarComponent = class AppSidebarComponent {
    constructor(layoutService, el) {
        this.layoutService = layoutService;
        this.el = el;
        this.timeout = null;
    }
    resetOverlay() {
        if (this.layoutService.state.overlayMenuActive) {
            this.layoutService.state.overlayMenuActive = false;
        }
    }
    get menuProfilePosition() {
        return this.layoutService.config.menuProfilePosition;
    }
    onMouseEnter() {
        if (!this.layoutService.state.anchored) {
            if (this.timeout) {
                clearTimeout(this.timeout);
                this.timeout = null;
            }
            this.layoutService.state.revealMenuActive = true;
        }
    }
    onMouseLeave() {
        if (!this.layoutService.state.anchored) {
            if (!this.timeout) {
                this.timeout = setTimeout(() => this.layoutService.state.revealMenuActive = false, 300);
            }
        }
    }
    anchor() {
        this.layoutService.state.anchored = !this.layoutService.state.anchored;
    }
    ngOnDestroy() {
        this.resetOverlay();
    }
};
__decorate([
    ViewChild(AppMenuProfileComponent)
], AppSidebarComponent.prototype, "menuProfile", void 0);
__decorate([
    ViewChild('menuContainer')
], AppSidebarComponent.prototype, "menuContainer", void 0);
AppSidebarComponent = __decorate([
    Component({
        selector: 'app-sidebar',
        templateUrl: './app.sidebar.component.html'
    })
], AppSidebarComponent);
export { AppSidebarComponent };
//# sourceMappingURL=app.sidebar.component.js.map