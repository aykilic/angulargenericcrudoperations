import { __decorate } from "tslib";
import { Component, HostBinding, Input } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { filter } from 'rxjs/operators';
let AppMenuitemComponent = class AppMenuitemComponent {
    constructor(layoutService, cd, router, menuService, appSidebar) {
        this.layoutService = layoutService;
        this.cd = cd;
        this.router = router;
        this.menuService = menuService;
        this.appSidebar = appSidebar;
        this.active = false;
        this.key = "";
        this.menuSourceSubscription = this.menuService.menuSource$.subscribe(value => {
            Promise.resolve(null).then(() => {
                if (value.routeEvent) {
                    this.active = (value.key === this.key || value.key.startsWith(this.key + '-')) ? true : false;
                }
                else {
                    if (value.key !== this.key && !value.key.startsWith(this.key + '-')) {
                        this.active = false;
                    }
                }
            });
        });
        this.menuResetSubscription = this.menuService.resetSource$.subscribe(() => {
            this.active = false;
        });
        this.router.events.pipe(filter(event => event instanceof NavigationEnd))
            .subscribe(params => {
            if (this.isSlim || this.isHorizontal) {
                this.active = false;
            }
            else {
                if (this.item.routerLink) {
                    this.updateActiveStateFromRoute();
                }
            }
        });
    }
    ngOnInit() {
        this.key = this.parentKey ? this.parentKey + '-' + this.index : String(this.index);
        if (!(this.isSlim || this.isHorizontal) && this.item.routerLink) {
            this.updateActiveStateFromRoute();
        }
    }
    updateActiveStateFromRoute() {
        let activeRoute = this.router.isActive(this.item.routerLink[0], { paths: 'exact', queryParams: 'ignored', matrixParams: 'ignored', fragment: 'ignored' });
        if (activeRoute) {
            this.menuService.onMenuStateChange({ key: this.key, routeEvent: true });
        }
    }
    onSubmenuAnimated(event) {
        var _a, _b;
        if (event.toState === 'visible' && this.layoutService.isDesktop() && (this.layoutService.isHorizontal() || this.layoutService.isSlim)) {
            const el = event.element;
            const container = this.appSidebar.menuContainer.nativeElement;
            if (this.layoutService.isHorizontal()) {
                const scrollLeft = container.scrollLeft;
                const offsetLeft = ((_a = el.parentElement) === null || _a === void 0 ? void 0 : _a.offsetLeft) || 0;
                el.style.left = (offsetLeft - scrollLeft) + 'px';
            }
            else if (this.layoutService.isSlim()) {
                const scrollTop = container.scrollTop;
                const offsetTop = ((_b = el.parentElement) === null || _b === void 0 ? void 0 : _b.offsetTop) || 0;
                el.style.top = (offsetTop - scrollTop) + 'px';
            }
        }
    }
    itemClick(event) {
        // avoid processing disabled items
        if (this.item.disabled) {
            event.preventDefault();
            return;
        }
        // navigate with hover
        if (this.root && this.isSlim || this.isHorizontal) {
            this.layoutService.state.menuHoverActive = !this.layoutService.state.menuHoverActive;
        }
        // execute command
        if (this.item.command) {
            this.item.command({ originalEvent: event, item: this.item });
        }
        // toggle active state
        if (this.item.items) {
            this.active = !this.active;
            if (this.root && this.active && (this.isSlim || this.isHorizontal)) {
                this.layoutService.onOverlaySubmenuOpen();
            }
        }
        else {
            if (this.layoutService.isMobile()) {
                this.layoutService.state.staticMenuMobileActive = false;
            }
            if (this.isSlim || this.isHorizontal) {
                this.menuService.reset();
                this.layoutService.state.menuHoverActive = false;
            }
        }
        this.menuService.onMenuStateChange({ key: this.key });
    }
    onMouseEnter() {
        // activate item on hover
        if (this.root && (this.isSlim || this.isHorizontal) && this.layoutService.isDesktop()) {
            if (this.layoutService.state.menuHoverActive) {
                this.active = true;
                this.menuService.onMenuStateChange({ key: this.key });
            }
        }
    }
    get submenuAnimation() {
        if (this.layoutService.isDesktop() && (this.layoutService.isHorizontal() || this.layoutService.isSlim()))
            return this.active ? 'visible' : 'hidden';
        else
            return this.root ? 'expanded' : (this.active ? 'expanded' : 'collapsed');
    }
    get isHorizontal() {
        return this.layoutService.isHorizontal();
    }
    get isSlim() {
        return this.layoutService.isSlim();
    }
    get activeClass() {
        return this.active && !this.root;
    }
    ngOnDestroy() {
        if (this.menuSourceSubscription) {
            this.menuSourceSubscription.unsubscribe();
        }
        if (this.menuResetSubscription) {
            this.menuResetSubscription.unsubscribe();
        }
    }
};
__decorate([
    Input()
], AppMenuitemComponent.prototype, "item", void 0);
__decorate([
    Input()
], AppMenuitemComponent.prototype, "index", void 0);
__decorate([
    Input(),
    HostBinding('class.layout-root-menuitem')
], AppMenuitemComponent.prototype, "root", void 0);
__decorate([
    Input()
], AppMenuitemComponent.prototype, "parentKey", void 0);
__decorate([
    HostBinding('class.active-menuitem')
], AppMenuitemComponent.prototype, "activeClass", null);
AppMenuitemComponent = __decorate([
    Component({
        // eslint-disable-next-line @angular-eslint/component-selector
        selector: '[app-menuitem]',
        template: `
		<ng-container>
            <div *ngIf="root && item.visible !== false" class="layout-menuitem-root-text">
                <span>{{item.label}}</span>
                <i class="layout-menuitem-root-icon pi pi-fw pi-ellipsis-h"></i>
            </div>
			<a *ngIf="(!item.routerLink || item.items) && item.visible !== false" [attr.href]="item.url" (click)="itemClick($event)"  (mouseenter)="onMouseEnter()"
			   [ngClass]="item.class" [attr.target]="item.target" tabindex="0" pRipple [pTooltip]="item.label" [tooltipDisabled]="!(isSlim && root && !active)">
				<i [ngClass]="item.icon" class="layout-menuitem-icon"></i>
				<span class="layout-menuitem-text">{{item.label}}</span>
				<i class="pi pi-fw pi-angle-down layout-submenu-toggler" *ngIf="item.items"></i>
			</a>
			<a *ngIf="(item.routerLink && !item.items) && item.visible !== false" (click)="itemClick($event)" (mouseenter)="onMouseEnter()" [ngClass]="item.class" 
			   [routerLink]="item.routerLink" routerLinkActive="active-route" [routerLinkActiveOptions]="item.routerLinkActiveOptions||{ paths: 'exact', queryParams: 'ignored', matrixParams: 'ignored', fragment: 'ignored' }"
               [fragment]="item.fragment" [queryParamsHandling]="item.queryParamsHandling" [preserveFragment]="item.preserveFragment" 
               [skipLocationChange]="item.skipLocationChange" [replaceUrl]="item.replaceUrl" [state]="item.state" [queryParams]="item.queryParams"
               [attr.target]="item.target" tabindex="0" pRipple [pTooltip]="item.label" [tooltipDisabled]="!(isSlim && root)">
				<i [ngClass]="item.icon" class="layout-menuitem-icon"></i>
				<span class="layout-menuitem-text">{{item.label}}</span>
				<i class="pi pi-fw pi-angle-down layout-submenu-toggler" *ngIf="item.items"></i>
			</a>

			<ul *ngIf="item.items && item.visible !== false" [@children]="submenuAnimation" (@children.done)="onSubmenuAnimated($event)">
				<ng-template ngFor let-child let-i="index" [ngForOf]="item.items">
					<li app-menuitem [item]="child" [index]="i" [parentKey]="key" [class]="child.badgeClass"></li>
				</ng-template>
			</ul>
		</ng-container>
    `,
        animations: [
            trigger('children', [
                state('collapsed', style({
                    height: '0'
                })),
                state('expanded', style({
                    height: '*'
                })),
                state('hidden', style({
                    display: 'none'
                })),
                state('visible', style({
                    display: 'block'
                })),
                transition('collapsed <=> expanded', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
            ])
        ]
    })
], AppMenuitemComponent);
export { AppMenuitemComponent };
//# sourceMappingURL=app.menuitem.component.js.map