import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
let LayoutService = class LayoutService {
    constructor() {
        this.config = {
            ripple: true,
            inputStyle: 'outlined',
            menuMode: 'static',
            colorScheme: 'light',
            componentTheme: 'indigo',
            scale: 14,
            menuTheme: 'light',
            topbarTheme: 'indigo',
            menuProfilePosition: 'start'
        };
        this.state = {
            staticMenuDesktopInactive: false,
            overlayMenuActive: false,
            configSidebarVisible: false,
            staticMenuMobileActive: false,
            menuHoverActive: false,
            rightMenuActive: false,
            topbarMenuActive: false,
            menuProfileActive: false,
            revealMenuActive: false,
            anchored: false
        };
        this.configUpdate = new Subject();
        this.overlayOpen = new Subject();
        this.topbarMenuOpen = new Subject();
        this.menuProfileOpen = new Subject();
        this.configUpdate$ = this.configUpdate.asObservable();
        this.overlayOpen$ = this.overlayOpen.asObservable();
        this.topbarMenuOpen$ = this.topbarMenuOpen.asObservable();
        this.menuProfileOpen$ = this.menuProfileOpen.asObservable();
    }
    onMenuToggle() {
        if (this.isOverlay()) {
            this.state.overlayMenuActive = !this.state.overlayMenuActive;
            if (this.state.overlayMenuActive) {
                this.overlayOpen.next(null);
            }
        }
        if (this.isDesktop()) {
            this.state.staticMenuDesktopInactive = !this.state.staticMenuDesktopInactive;
        }
        else {
            this.state.staticMenuMobileActive = !this.state.staticMenuMobileActive;
            if (this.state.staticMenuMobileActive) {
                this.overlayOpen.next(null);
            }
        }
    }
    onTopbarMenuToggle() {
        this.state.topbarMenuActive = !this.state.topbarMenuActive;
        if (this.state.topbarMenuActive) {
            this.topbarMenuOpen.next(null);
        }
    }
    onOverlaySubmenuOpen() {
        this.overlayOpen.next(null);
    }
    showConfigSidebar() {
        this.state.configSidebarVisible = true;
    }
    isOverlay() {
        return this.config.menuMode === 'overlay';
    }
    isDesktop() {
        return window.innerWidth > 991;
    }
    isSlim() {
        return this.config.menuMode === 'slim';
    }
    isHorizontal() {
        return this.config.menuMode === 'horizontal';
    }
    isMobile() {
        return !this.isDesktop();
    }
    onConfigUpdate() {
        this.configUpdate.next(this.config);
    }
    isRightMenuActive() {
        return this.state.rightMenuActive;
    }
    openRightSidebar() {
        this.state.rightMenuActive = true;
    }
    onMenuProfileToggle() {
        this.state.menuProfileActive = !this.state.menuProfileActive;
        if (this.state.menuProfileActive && this.isHorizontal() && this.isDesktop()) {
            this.menuProfileOpen.next(null);
        }
    }
    replaceThemeLink(href, onComplete) {
        const id = 'theme-link';
        const themeLink = document.getElementById(id);
        const cloneLinkElement = themeLink.cloneNode(true);
        cloneLinkElement.setAttribute('href', href);
        cloneLinkElement.setAttribute('id', id + '-clone');
        themeLink.parentNode.insertBefore(cloneLinkElement, themeLink.nextSibling);
        cloneLinkElement.addEventListener('load', () => {
            themeLink.remove();
            cloneLinkElement.setAttribute('id', id);
            onComplete();
        });
    }
    onColorSchemeChange(colorScheme) {
        const themeLink = document.getElementById('theme-link');
        const themeLinkHref = themeLink.getAttribute('href');
        const currentColorScheme = 'theme-' + this.config.colorScheme;
        const newColorScheme = 'theme-' + colorScheme;
        const newHref = themeLinkHref.replace(currentColorScheme, newColorScheme);
        this.replaceThemeLink(newHref, () => {
            this.config.colorScheme = colorScheme;
            if (colorScheme === 'dark') {
                this.config.menuTheme = 'dark';
            }
            this.onConfigUpdate();
        });
    }
};
LayoutService = __decorate([
    Injectable({
        providedIn: 'root',
    })
], LayoutService);
export { LayoutService };
//# sourceMappingURL=app.layout.service.js.map