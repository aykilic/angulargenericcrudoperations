import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
let AppTopbarComponent = class AppTopbarComponent {
    constructor(layoutService, el) {
        this.layoutService = layoutService;
        this.el = el;
        this.model = [
            {
                label: 'UI KIT',
                items: [
                    [
                        {
                            label: 'UI KIT 1',
                            items: [
                                { label: 'Form Layout', icon: 'pi pi-fw pi-id-card', routerLink: ['/uikit/formlayout'] },
                                { label: 'Input', icon: 'pi pi-fw pi-check-square', routerLink: ['/uikit/input'] },
                                { label: 'Float Label', icon: 'pi pi-fw pi-bookmark', routerLink: ['/uikit/floatlabel'] },
                                { label: 'Button', icon: 'pi pi-fw pi-mobile', routerLink: ['/uikit/button'] },
                                { label: 'File', icon: 'pi pi-fw pi-file', routerLink: ['/uikit/file'] }
                            ]
                        }
                    ],
                    [
                        {
                            label: 'UI KIT 2',
                            items: [
                                { label: 'Table', icon: 'pi pi-fw pi-table', routerLink: ['/uikit/table'] },
                                { label: 'List', icon: 'pi pi-fw pi-list', routerLink: ['/uikit/list'] },
                                { label: 'Tree', icon: 'pi pi-fw pi-share-alt', routerLink: ['/uikit/tree'] },
                                { label: 'Panel', icon: 'pi pi-fw pi-tablet', routerLink: ['/uikit/panel'] },
                                { label: 'Chart', icon: 'pi pi-fw pi-chart-bar', routerLink: ['/uikit/charts'] }
                            ]
                        }
                    ],
                    [
                        {
                            label: 'UI KIT 3',
                            items: [
                                { label: 'Overlay', icon: 'pi pi-fw pi-clone', routerLink: ['/uikit/overlay'] },
                                { label: 'Media', icon: 'pi pi-fw pi-image', routerLink: ['/uikit/media'] },
                                { label: 'Menu', icon: 'pi pi-fw pi-bars', routerLink: ['/uikit/menu'] },
                                { label: 'Message', icon: 'pi pi-fw pi-comment', routerLink: ['/uikit/message'] },
                                { label: 'Misc', icon: 'pi pi-fw pi-circle-off', routerLink: ['/uikit/misc'] }
                            ]
                        }
                    ]
                ]
            },
            {
                label: 'UTILITIES',
                items: [
                    [
                        {
                            label: 'UTILITIES 1',
                            items: [
                                { label: 'Icons', icon: 'pi pi-fw pi-prime', routerLink: ['utilities/icons'] },
                                { label: 'PrimeFlex', icon: 'pi pi-fw pi-desktop', url: 'https://www.primefaces.org/primeflex/', target: '_blank' }
                            ]
                        }
                    ],
                ]
            }
        ];
    }
    get mobileTopbarActive() {
        return this.layoutService.state.topbarMenuActive;
    }
    onMenuButtonClick() {
        this.layoutService.onMenuToggle();
    }
    onRightMenuButtonClick() {
        this.layoutService.openRightSidebar();
    }
    onMobileTopbarMenuButtonClick() {
        this.layoutService.onTopbarMenuToggle();
    }
    focusSearchInput() {
        setTimeout(() => {
            this.searchInput.nativeElement.focus();
        }, 0);
    }
};
__decorate([
    ViewChild('menuButton')
], AppTopbarComponent.prototype, "menuButton", void 0);
__decorate([
    ViewChild('mobileMenuButton')
], AppTopbarComponent.prototype, "mobileMenuButton", void 0);
__decorate([
    ViewChild('searchInput')
], AppTopbarComponent.prototype, "searchInput", void 0);
AppTopbarComponent = __decorate([
    Component({
        selector: 'app-topbar',
        templateUrl: './app.topbar.component.html'
    })
], AppTopbarComponent);
export { AppTopbarComponent };
//# sourceMappingURL=app.topbar.component.js.map