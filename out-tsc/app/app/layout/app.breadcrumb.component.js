import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { NavigationEnd } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
let AppBreadcrumbComponent = class AppBreadcrumbComponent {
    constructor(router) {
        this.router = router;
        this._breadcrumbs$ = new BehaviorSubject([]);
        this.breadcrumbs$ = this._breadcrumbs$.asObservable();
        this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(event => {
            const root = this.router.routerState.snapshot.root;
            const breadcrumbs = [];
            this.addBreadcrumb(root, [], breadcrumbs);
            this._breadcrumbs$.next(breadcrumbs);
        });
    }
    addBreadcrumb(route, parentUrl, breadcrumbs) {
        const routeUrl = parentUrl.concat(route.url.map(url => url.path));
        const breadcrumb = route.data['breadcrumb'];
        const parentBreadcrumb = route.parent && route.parent.data ? route.parent.data['breadcrumb'] : null;
        if (breadcrumb && breadcrumb !== parentBreadcrumb) {
            breadcrumbs.push({
                label: route.data['breadcrumb'],
                url: '/' + routeUrl.join('/')
            });
        }
        if (route.firstChild) {
            this.addBreadcrumb(route.firstChild, routeUrl, breadcrumbs);
        }
    }
};
AppBreadcrumbComponent = __decorate([
    Component({
        selector: 'app-breadcrumb',
        templateUrl: './app.breadcrumb.component.html'
    })
], AppBreadcrumbComponent);
export { AppBreadcrumbComponent };
//# sourceMappingURL=app.breadcrumb.component.js.map