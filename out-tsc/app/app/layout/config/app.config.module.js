import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SidebarModule } from 'primeng/sidebar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ButtonModule } from 'primeng/button';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AppConfigComponent } from './app.config.component';
let AppConfigModule = class AppConfigModule {
};
AppConfigModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            SidebarModule,
            RadioButtonModule,
            ButtonModule,
            InputSwitchModule
        ],
        declarations: [
            AppConfigComponent
        ],
        exports: [
            AppConfigComponent
        ]
    })
], AppConfigModule);
export { AppConfigModule };
//# sourceMappingURL=app.config.module.js.map