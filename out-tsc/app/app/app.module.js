import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppLayoutModule } from './layout/app.layout.module';
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HomeModule } from "./works/components/page/home/home.module";
import { DynamicFormComponent } from "./works/components/dynamicComponent/dynamicFormComponent";
import { DynamicSelectComponent } from "./works/components/dynamicComponent/dynamicField/dynamicSelect/dynamicSelectComponent";
let AppModule = class AppModule {
};
AppModule = __decorate([
    NgModule({
        declarations: [
            AppComponent
        ],
        imports: [
            AppRoutingModule,
            AppLayoutModule,
            BrowserModule,
            HttpClientModule,
            FormsModule,
            ReactiveFormsModule,
            HomeModule,
            DynamicFormComponent,
            DynamicSelectComponent,
        ],
        providers: [
            { provide: LocationStrategy, useClass: PathLocationStrategy }
        ],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map