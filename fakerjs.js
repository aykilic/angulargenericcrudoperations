
import { faker } from '@faker-js/faker';
import fs from 'fs';

function generateUsers() {

    let users = []

    for (let id=1; id <= 200000; id++) {

        let firstName = faker.name.firstName();
        let lastName = faker.name.lastName();
        let email = faker.internet.email();
        let birthday = faker.date.birthdate();
        let company = faker.company.name();
        let phone =faker.phone.number();

        users.push({
            "id": id,
            "first_name": firstName,
            "last_name": lastName,
            "email": email,
        });
    }

    return { "users": users }
}

let dataObj = generateUsers();

fs.writeFileSync('db.json', JSON.stringify(dataObj, null, '\t'))